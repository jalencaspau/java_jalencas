
public class Conversor {

    public double mih2kmh(double mih){
        return mih*1.6;
    }

    public double kmh2ms(double kmh){
        return kmh*0.28;
    }
    
}