
class Test {

   public static void main(String[] args) {
      Conversor conversor1 = new Conversor();

      double millasHora = 1;
      double kmh = conversor1.mih2kmh(millasHora);
      System.out.printf("%f millas/h equivalen a %f km/h.\n", millasHora, kmh);

      double kilometrosHora = 1;
      double ms = conversor1.kmh2ms(kilometrosHora);
      System.out.printf("%f km/h equivalen a %f m/s.\n", kilometrosHora, ms);

   }

}